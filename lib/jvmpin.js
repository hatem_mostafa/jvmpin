/**
 * JVMPin - Nailgun protocol implementation
 *
 * A simple node based implementation of the Nailgun protocol for communicating
 * with a JVM hosting a Nailgun server instance.
 *
 * @since 1.0.0
 *
 * EPL Licensed
 */
var util         = require('util'),
    net          = require('net'),
    events       = require('events'),
    BufferStream = require('bufferstream');

/**
 * createConnection([port], [host], [connectionCallback])
 *
 * Factory function to create a communication socket to the nailgun host and
 * bind it to a new JVMPin instance. By default it sets the port to 2113 and
 * the host to 'localhost'.
 *
 * usage:
 *    require('jvmpin').createConnection(1234, 'some.host', function() {
 *      console.log('connected');
 *    });
 *
 * The API follows that used by the the net.Socket Class.
 * @see http://nodejs.org/api/net.html#net_class_net_socket for more details.
 *
 * @param port - Number.   The port number to connect to the nailgun jvm instance.
 * @param host - String.   The hostname to use when connecting to the nailgun jvm instance.
 * @param cb   - Function. A callback function to invoke on connection.
 */
exports.createConnection = function() {
	// test arguments for defaulting the host/port
	var port = arguments[0] || 2113;
	    host = arguments[1] || 'localhost';

	return new JVMPin(net.createConnection(port, host, arguments[2]));
};

/**
 * new JVMPin(socket)
 *
 * The JVMPin instance can be viewed as an instance of a socket specialized for
 * communication to a Nailgun hosted jvm instance. All events used by the internal
 * net.Socket are proxied via this class.
 *
 * @param socket - Socket. An instance of a socket to communicate over.
 */
function JVMPin(socket) {
	JVMPin.super_.apply(this, arguments);

	// The communication channel to the Nailgun JVM instance.
	this._socket = socket;

	// Data events can be emitted without a complete chunk hence a simple buffer is used.
	this._unprocessedBuffer = new Buffer(0);

	// Whenever a listener is registered to this object it will be forwarded to the socket.
	var self = this;
	this.on('newListener', function(event, listener) {
		self._socket.on(event, listener);
	});

	this.CHUNK_TYPE = {
		ARGUMENT:          'A',
		ENVIRONMENT:       'E',
		WORKING_DIRECTORY: 'D',
		COMMAND:           'C',
		STDIN:             '0',
		STDOUT:            '1',
		STDERR:            '2',
		EOF:               '.',
		EXIT:              'X'
	};

	/**
	 * The JVMPinProcess is basically dumb terminal. It is used to perform stdio
	 * redirection and propagate the appropriate signals for each. The API is akin
	 * to that found in the 'child_process' API only that none of the overhead of
	 * forking/executing/spawning occurs.
	 *
	 * @since 1.0.6
	 */
	function JVMPinProcess() {
		JVMPin.super_.apply(this, arguments);

		this.stdin = new BufferStream();
		this.stdout = new BufferStream();
		this.stderr = new BufferStream();

		this._killHasBeenCalled = false;

		var self = this;

		/**
		 * A faux 'kill' function which emits the required signals to ensure that
		 * all bound objects remove themselves as appropriate. For all intents and
		 * purposes this should very much should be deemed a deconstructor.
		 *
		 * @param signal - Number. process exit code provided by the ng-server.
		 */
		this.kill = function(signal) {
			// Prevent process from being killed twice. Nailgun
			// seems to like to send two exit chunks instead of
			// just one.
			if (self._killHasBeenCalled) return;
			self._killHasBeenCalled = true;

			signal = signal || 0;
			self.emit('exit', signal);
			self.stdin.emit('close')
			self.stdin.disable();
			self.stdout.end();
			self.stdout.disable();
			self.stderr.end();
			self.stderr.disable();
		}
	}
	util.inherits(JVMPinProcess, events.EventEmitter);

	/**
	 * readChunks(bufferData)
	 *
	 * Reads the passed in bufferData and processes any complete chunks. All unprocessed
	 * chunk data is then moved to the _unprocessedBuffer.
	 *
	 * NOTE:
	 * This could use some optimizations by promoting a mixture of concat and a pointer
	 * arithmetic for writing directly into the buffer. (perhaps look at using a stream)
	 *
	 * @param bufferData - Buffer. A buffer object containing raw packet data to process.
	 * @return Array of Objects with 'type' and 'data' properties. The type property is
	 *          a CHUNK_TYPE identifier while the data is the processed chunk data.
	 */
	this.readChunks = function(bufferData) {
		self._unprocessedBuffer = Buffer.concat([self._unprocessedBuffer, bufferData]);

		if (self._unprocessedBuffer.length < 5) { // need more before reading chunk head
			return [];
		}

		var chunkSize = self._unprocessedBuffer.readUInt32BE(0),
		    chunkCode = self._unprocessedBuffer.toString('ascii', 4, 5);

		if (chunkSize + 5 > self._unprocessedBuffer.length) { // need more before reading chunk data
			return [];
		}

		var chunkEnd  = 5 + chunkSize,
		    chunkData = self._unprocessedBuffer.slice(5, chunkEnd),
		    unprocessedChunkData = self._unprocessedBuffer.slice(chunkEnd);

		self._unprocessedBuffer = self._unprocessedBuffer.slice(0, 0); // drain the _unprocessedBuffer.

		if (unprocessedChunkData.length > 0) {
			return [{ type: chunkCode, data: chunkData }].concat(self.readChunks(unprocessedChunkData));
		}

		return [{ type: chunkCode, data: chunkData }];
	};

	/**
	 * writeChunk(chunkType, data, [callback])
	 *
	 * Writes data to the socket using the nailgun chunk protocol data.
	 *
	 * @param chunkType - CHUNK_TYPE. Sets the chunk type identifier.
	 * @param data      - String | Buffer. Sets the chunk data. (with ascii encoding).
	 * @param cb        - Function. Called when the data is written to the socket.
	 * @return Boolean. If the full chunk is written returns true, otherwise false.
	 */
	this.writeChunk = function (chunkType, data, cb) {
		var chunkHead = new Buffer(5);
		    chunkData = (typeof data === 'string') ? new Buffer(data) : data;

		chunkHead.writeUInt32BE(data.length, 0);
		chunkHead.write(chunkType, 4, 'ascii');

		return self._socket.write(Buffer.concat([chunkHead, chunkData]), cb);
	};

	/**
	 * spawn(command, [args], [options])
	 *
	 * @param command - String. The 'nail' or main java class to execute.
	 * @param args    - Array. A list of arguments to send the command. (empty by default)
	 * @param options - Object A set of possible options to send to the process. Object keys
	 *                  of note are the 'cwd' which default to process.cwd() and 'env' which
	 *                  defaults to process.env.
	 */
	this.spawn = function(command, args, options) {
		if (typeof command !== 'string') {
			throw new Error("Unable to spawn command: ", command);
		}

		var args = args || []
		    options = options || {},
		    options.exitOnClose = options.exitOnClose || true;
		    options.env = options.env || process.env,
		    options.cwd = options.cwd || process.cwd();

		// protocol handshake
		args.forEach(function(arg) {
			self.writeChunk(self.CHUNK_TYPE.ARGUMENT, arg);
		});
		self.writeChunk(self.CHUNK_TYPE.ENVIRONMENT, 'NAILGUN_FILESEPARATOR=' + (require('os').type() === 'Windows_NT' ? ';' : ':'));
		self.writeChunk(self.CHUNK_TYPE.ENVIRONMENT, 'NAILGUN_PATHSEPARATOR=' + require('path').sep);
		for (key in options.env) {
			self.writeChunk(self.CHUNK_TYPE.ENVIRONMENT, key + '=' + options.env[key]);
		}
		self.writeChunk(self.CHUNK_TYPE.WORKING_DIRECTORY, options.cwd);
		self.writeChunk(self.CHUNK_TYPE.COMMAND, command);

		var jvmpin_process = new JVMPinProcess();
		self._socket.on('data', function(data) {
			var chunk = self.readChunks(data).forEach(function(chunk) {
				switch (chunk.type) {
					case self.CHUNK_TYPE.STDOUT:
						jvmpin_process.stdout.write(chunk.data);
						break;
					case self.CHUNK_TYPE.STDERR:
						jvmpin_process.stderr.write(chunk.data);
						break;
					case self.CHUNK_TYPE.EXIT:
						jvmpin_process.kill(chunk.data.toString());
						break;
					default:
						console.error("Unexpected chunk type", chunk.type, chunk.data.toString());
				}
			});
		});

		jvmpin_process.stdin.on('data', function(data) {
			self.writeChunk(self.CHUNK_TYPE.STDIN, data);
		}).on('end', function() {
			self.writeChunk(self.CHUNK_TYPE.EOF, "");
		});

		return jvmpin_process;
	};
}
util.inherits(JVMPin, events.EventEmitter);
